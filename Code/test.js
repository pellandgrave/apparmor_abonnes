var express = require('express');
var app = express();
var ldap = require('ldapjs');
const fs = require('fs');
const employeeAffiliationCSV = 'Employee';
var arrayEtudiants = [];
var arrayEmployes = [];
var writeStreamSortie = fs.createWriteStream('abonnesAppArmor.csv');

//Connexion locale sur le port 3000
app.listen(3000, function () {
    console.log("Démarrage du serveur")
})

//Connexion string au serveur OpenDJ
var client = ldap.createClient({
    url: 'ldaps://opendj.uqam.ca:636'
});

//Fonction appelée à l'exécution
//IMPORTANT : Utiliser un compte de service en crédentiels
function authentificationDN(username, password)
{
    //C'est ici qu'on tente la connexion
    client.bind(username, password, function (err) {
        if (err) {
            //Si la connexion a échoué, on log le message d'erreur
            console.log("Erreur dans la connexion " + err)
        } else {
            //Si la connexion a fonctionné
            console.log("Succès");
            //On lance la première recherche pour les étudiants actifs
            searchEtudiants();
        }
    });


}

function searchEtudiants() {


    var optsEtudiants = {
        //Filtre
        //On veut les personnes ayant comme affiliation principale la valeur "student"
        filter: '(eduPersonPrimaryAffiliation=student)',
        //Portée Sous-arbre
        scope: 'sub',
        //On importe seulement les attributs nécessaires
        attributes: ['cn','sn', 'givenName', 'eduPersonPrimaryAffiliation', 'eduPersonPrincipalName']
    };
    
    //Lancer la recherche
    //Spécifier la base de recherche dans OpenDJ ainsi que les filtres/attributs/portées
    client.search('ou=personnes,dc=code,dc=uqam,dc=ca', optsEtudiants, function (err, resEtudiants) {


        if (err) {
            console.log("Erreur dans la recherche " + err)
        } else {
            resEtudiants.on('searchEntry', function (entry) {
                //C'est l'entrées qui nous intéresse
                //On pousse l'objet JSON concernant l'étudiant dans une liste
                arrayEtudiants.push(entry.object);
                
            });
            resEtudiants.on('searchReference', function (referral) {
                console.log('Référence: ' + referral.uris.join());
            });
            resEtudiants.on('error', function (err) {
                console.error('Erreur: ' + err.message);
            });
            resEtudiants.on('end', function (result) {
                //Si on entre ici, c'est que la recherche est terminée.
                //On lance la recherche d'employés
                //IMPORTANT : On doit mettre les actions subséquentes à la recherche courant dans le 'end' et non à la suite dans authentificationDN, car fonction asynchrone.
                console.log('Recherche des étudiants actifs terminée. Status: ' + result.status);
                searchEmployes();
            });
        }
    });

}


function searchEmployes() {

    
    var optsEmployes = {
        //Filtre
        //On veut les personnes ayant comme affiliation principale la valeur "employee"
        filter: '(eduPersonAffiliation=employee)',
        //Portée Sous-arbre
        scope: 'sub',
        //On importe seulement les attributs nécessaires
        attributes: ['cn','sn', 'mail','givenName', 'eduPersonPrincipalName']
    };
    
    //Lancer la recherche
    //Spécifier la base de recherche dans OpenDJ ainsi que les filtres/attributs/portées
    client.search('ou=personnes,dc=code,dc=uqam,dc=ca', optsEmployes, function (err, resEmployes) {


        if (err) {
            console.log("Erreur dans la recherche " + err)
        } else {
            resEmployes.on('searchEntry', function (entry) {
                //C'est l'entrées qui nous intéresse
                //On pousse l'objet JSON concernant l'employé dans une liste
                arrayEmployes.push(entry.object);
                
            });
            resEmployes.on('searchReference', function (referral) {
                console.log('Référence: ' + referral.uris.join());
            });
            resEmployes.on('error', function (err) {
                console.error('Erreur: ' + err.message);
            });
            resEmployes.on('end', function (result) {
                //Si on entre ici, c'est que la recherche est terminée.
                //On lance la création du CSV
                //IMPORTANT : On doit mettre les actions subséquentes à la recherche courant dans le 'end' et non à la suite dans authentificationDN, car fonction asynchrone.
                console.log('Recherche des employés actifs terminée. Status: ' + result.status);
                creerCSV();
                console.log('Le fichier CSV ' +writeStreamSortie.path +' a été crée.');
            });
        }
    });

}


function creerCSV()
{
    //Première ligne en dur pour les headers que AppArmor souhaite recevoir
    ligneFichier = "FirstName,LastName,Mail,SSO ID,Phone Number,Affiliation";

    writeStreamSortie.write(ligneFichier);

    //On boucle à travers la liste d'étudiants pour entrer chaque étudiant dans le CSV
    for (let j = 0; j < arrayEtudiants.length; j++)
    {
        //FirstName = givenName
        //LastName = sn
        //Mail = eduPersonPrincipalName
        //SSO ID = eduPersonPrincipalName
        //Affiliation = eduPersonPrimaryAffiliation
        ligneFichier =  '"'/*+arrayEtudiants[j]['cn'] +'","'*/ +arrayEtudiants[j]['givenName'] + '","' + arrayEtudiants[j]['sn'] + '","' + arrayEtudiants[j]['eduPersonPrincipalName'] + '","'  + arrayEtudiants[j]['eduPersonPrincipalName'] + '",,"'+arrayEtudiants[j]['eduPersonPrimaryAffiliation'] + '"';
        
        writeStreamSortie.write('\n'+ligneFichier);

    }

    //On boucle à travers la liste d'd'employés pour entrer chaque employé dans le CSV
    for (let j = 0; j < arrayEmployes.length; j++)
    {
        //FirstName = givenName
        //LastName = sn
        //Mail = mail
        //SSO ID = eduPersonPrincipalName
        //Affiliation = Employee en dur, car on peut avoir Staff, Employee et Faculty.... pas de distinction dans AppArmor
        ligneFichier =  '"'/*+arrayEmployes[j]['cn'] +'","'*/ +arrayEmployes[j]['givenName'] + '","' + arrayEmployes[j]['sn'] + '","' + arrayEmployes[j]['mail'] + '","' + arrayEmployes[j]['mail'] + '",,"' +employeeAffiliationCSV+ '"';
        
        writeStreamSortie.write('\n'+ligneFichier);

    }

    //On ferme le file stream
    writeStreamSortie.close();  

}
//Appel de la fonction à l'exécution
authentificationDN("cn=app-armor,ou=services,dc=code,dc=uqam,dc=ca", "5Yh7PlfV#62QiS|wanHfDd");

//process.exit(1);